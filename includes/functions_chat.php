<?php

    use CHAT_QUERIES\Chat_Queries;

	function sendmailscript( ){
		global $wpdb, $FULLNAME, $FILECONTS;
		
		$current_user = wp_get_current_user( );
		
		$CLIENTID 	= $_POST[ 'contentz' ];
		//$MCONTENTS 	= "HELLO WORLD!";
		$FILESOURCE	= OF_FILEPATH . '/js/clients/code/'.$CLIENTID.'.txt';
		
		$FILECONTS 	= file_get_contents($FILESOURCE);
		
		$USEREMAIL	= $current_user->user_email;
		$USERFNAME	= $current_user->user_firstname;
		$USERLNAME	= $current_user->user_lastname;
		$USERUNAME 	= $current_user->user_login;
		$FULLNAME   = $USERFNAME . ' ' . $USERLNAME;
		$ADMINEMAIL = get_option( 'admin_email' );
		$TITLE		= get_field('email_subject','option');
		//$TITLE		= "Sample Title";
		$FROMNAME	= get_field('email_name','option');
		
		$MESSAGEC	= get_field('email_message','option');
		
		$TO			= $USEREMAIL;
		$HEADERS	= 'From: ' . $FROMNAME . ' <' . $ADMINEMAIL . '>' . "\r\n";
		$ECONTENT	= $MESSAGEC;
		//$ECONTENT	= "Sample Message";
		add_filter( 'wp_mail_content_type', 'set_html_content_type' );
		
		$STATUS 	= wp_mail($TO, $TITLE, $ECONTENT, $HEADERS);
		remove_filter( 'wp_mail_content_type', 'set_html_content_type' );
 		 
		if($STATUS){
			echo 'SUCCESS';
		}else{
			echo 'FAILED';
		}  
		die( ); 
		
	}
	add_action('wp_ajax_sendmailscript', 'sendmailscript');
	
	function processwidget( ){
		global $wpdb;
		$ERR_MSG_NOICON		= "Live Chat Icons must be selected.";
		$ERR_MSG_NOCICON	= "Custom Live Chat Icons must have valid Image URL.";
		$ERR_MSG_FAIL		= "1 Your chat has not made any changes. If you feel that this error needs help, please feel free to contact us.";
		$ERR_MSG_IMGNF		= "Custom Chat Icon does not have a valid Image URL.";
		$ERR_MSG_IMGSIZE	= "Custom Images must have 200px by 100px dimensions.";
		$ERR_MSG_NOTREG		= "Your account is not associated with our backend system.";
		$SUC_MSG_INSERT	 	= "Live Chat successfully created.";
		$SUC_MSG_UPDATE	 	= "Live Chat successfully updated.";	
		$P_V_CID		= $_POST[ 'p_clientid' ];
		$P_V_ICON 		= $_POST[ 'p_cicon' ];
		$P_V_CICON 		= $_POST[ 'p_obut' ];
		$P_V_CICOURLON 	= $_POST[ 'p_cctxt1' ];
		$P_V_CICOURLOFF = $_POST[ 'p_cctxt2' ];
		$P_V_CWINDOW 	= $_POST[ 'p_cwchat' ];
		$P_V_PACTIVE 	= $_POST[ 'p_cpactive' ];
		$P_V_POPEXIT 	= $_POST[ 'p_cexitpop' ];
		$CHECK_ID 		= $wpdb->get_row("SELECT * FROM " . $wpdb->prefix . "widgetoptions WHERE wid_accountid='" . $P_V_CID . "'");
		$response		= "";
		if(!$P_V_CID){
			$response = generate_response("error", $ERR_MSG_NOTREG);
		}elseif(empty($P_V_ICON) && $P_V_CICON == 0){
			$response = generate_response("error", $ERR_MSG_NOICON);
		}elseif($P_V_CICON == 1 && (empty($P_V_CICOURLON) || empty($P_V_CICOURLOFF))){
			$response = generate_response("error", $ERR_MSG_NOCICON);
		}else{
			if(empty($P_V_ICON) && (!empty($P_V_CICOURLON) || !empty($P_V_CICOURLOFF))){
				$IMG1 = @getimagesize($P_V_CICOURLON);
				$IMG2 = @getimagesize($P_V_CICOURLOFF);
				if(!$IMG1 || !$IMG2){
					$response = generate_response("error", $ERR_MSG_IMGNF);
				}elseif($IMG1[0] > 200 || $IMG1[1] > 100 || $IMG2[0] > 200 || $IMG2[1] > 100){
					$response = generate_response("error", $ERR_MSG_IMGSIZE);
				}else{
					$BTYPE = 'CUSTOM';
					//$CHECK_ID = $wpdb->get_row("SELECT * FROM " . $wpdb->prefix . "widgetoptions WHERE wid_accountid = " . $getName[0]->id);
					
					if(count($CHECK_ID) > 0){
						$update_data = array(
							'wid_imgpathon'		=> $P_V_CICOURLON,
							'wid_imgpathoff'	=> $P_V_CICOURLOFF,
							'wid_chattype'		=> $P_V_CWINDOW,
							'wid_proactive'		=> $P_V_PACTIVE,
							'wid_exitpop'		=> $P_V_POPEXIT,
							'wid_buttontype'	=> $BTYPE
						);
						$update_format = array(
							'%s',
							'%s',
							'%s',
							'%d',
							'%d',
							'%s'
						);
						
						$query_update = $wpdb->update($wpdb->prefix . "widgetoptions", $update_data, array('wid_accountid' => $P_V_CID), $update_format, array('%s'));
						if($query_update){
							$response = generate_response("success", $SUC_MSG_UPDATE);
						}else{
						    print " err1";
							$response = generate_response("error", $ERR_MSG_FAIL);
						}
					}else{
						$insert_data = array(
							'wid_accountid'		=> $P_V_CID,
							'wid_imgpathon'		=> $P_V_CICOURLON,
							'wid_imgpathoff'	=> $P_V_CICOURLOFF,
							'wid_chattype'		=> $P_V_CWINDOW,
							'wid_proactive'		=> $P_V_PACTIVE,
							'wid_exitpop'		=> $P_V_POPEXIT,
							'wid_buttontype'	=> $BTYPE
						);
						$insert_format = array(
							'%s',
							'%s',
							'%s',
							'%s',
							'%d',
							'%d',
							'%s'
						);
						$query_insert = $wpdb->insert($wpdb->prefix . 'widgetoptions', $insert_data, $insert_format);
						
						if($query_insert){
							$response = generate_response("success", $SUC_MSG_INSERT);
						}else{
                            print " err1";

                            $response = generate_response("error", $ERR_MSG_FAIL);
						}
					} 
					
				}
			}else{
				$BTYPE 		= 'DEFAULT';
				$IMG_ON 	= str_replace("CHATICON-OFF", "CHATICON-ON", $P_V_ICON);
				$IMG_OFF	= $P_V_ICON;
				if(count($CHECK_ID) > 0){
					
					$update_data = array(
						'wid_imgpathon'		=> $IMG_ON,
						'wid_imgpathoff'	=> $IMG_OFF,
						'wid_chattype'		=> $P_V_CWINDOW,
						'wid_proactive'		=> $P_V_PACTIVE,
						'wid_exitpop'		=> $P_V_POPEXIT,
						'wid_buttontype'	=> $BTYPE
					);
					$update_format = array(
						'%s',
						'%s',
						'%s',
						'%d',
						'%d',
						'%s'
					);
					
					$query_update = $wpdb->update($wpdb->prefix . "widgetoptions", $update_data, array('wid_accountid' => $P_V_CID), $update_format, array('%s'));
					if($query_update){
						$response = generate_response("success", $SUC_MSG_UPDATE);
					}else{
                        print " err1";

                        $response = generate_response("error", $ERR_MSG_FAIL);
					}
				}else{
					$insert_data = array(
						'wid_accountid'		=> $P_V_CID,
						'wid_imgpathon'		=> $IMG_ON,
						'wid_imgpathoff'	=> $IMG_OFF,
						'wid_chattype'		=> $P_V_CWINDOW,
						'wid_proactive'		=> $P_V_PACTIVE,
						'wid_exitpop'		=> $P_V_POPEXIT,
						'wid_buttontype'	=> $BTYPE
					);
					$insert_format = array(
						'%s',
						'%s',
						'%s',
						'%s',
						'%d',
						'%d',
						'%s'
					);
					$query_insert = $wpdb->insert($wpdb->prefix . 'widgetoptions', $insert_data, $insert_format);
					
					if($query_insert){
						$response = generate_response("success", $SUC_MSG_INSERT);
					}else{
                        print " err1";

                        $response = generate_response("error", $ERR_MSG_FAIL);
					}
				}
			}
		}
		echo $response;			
		die( );
	}


	add_action('wp_ajax_processwidget', 'processwidget');

	function process_icon_add($P_V_CID, $P_V_CICOURLON, $P_V_CICOURLOFF, $BTYPE) {

	    global $wpdb;
        // add here to icons library by jesus
        $add_icon_library	= $wpdb->insert($wpdb->prefix . 'chat_icons_library',
            [
                'partner_id'    => $P_V_CID,
                'online_icon'   => $P_V_CICOURLON,
                'offline_icon'	=> $P_V_CICOURLOFF,
                'byte'	        => $BTYPE
            ],
            [
                '%s',
                '%s',
                '%s',
                '%s'
            ]
        );

        return $add_icon_library;
    }


    function L121c_checkRemoteFile($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        // don't download content
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if(curl_exec($ch)!==FALSE)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

	function process_icons(){

	    print_r($_POST);

		global $wpdb;
		
		$ERR_MSG_NOICON		= "Please select a Live Chat Icons before you can proceed.";
		$ERR_MSG_NOCICON	= "Custom Live Chat Icons must have valid Image URL.";
		$ERR_MSG_FAIL		= "2 Your chat has not made any changes. If you feel that this error needs help, please feel free to contact us.";
		$ERR_MSG_IMGNF		= "Custom Chat Icon does not have a valid Image URL.";
		$ERR_MSG_IMGSIZE	= "Custom Images must have 200px by 100px dimensions.";
		$ERR_MSG_NOTREG		= "Your account is not associated with our backend system.";	
		
		$P_V_CID			=  (pnw_is_local() == true) ? 77333 : $_POST[ 'p_clientid' ];
		$P_V_ICON 			= $_POST[ 'p_cicon' ];
		$P_V_CICON 			= $_POST[ 'p_obut' ];
		$P_V_CICOURLON 		= $_POST[ 'p_cctxt1' ];
		$P_V_CICOURLOFF 	= $_POST[ 'p_cctxt2' ];


		$CHECK_ID 	     	= $wpdb->get_row("SELECT * FROM " . $wpdb->prefix . "chat_icons WHERE ci_accountid='" . $P_V_CID . "'");
		$response		    = "";






		// check if the new icon added is image or not


        if(!empty($P_V_CICOURLON) && !empty($P_V_CICOURLOFF)) {
            $img1 = L121c_checkRemoteFile($P_V_CICOURLON);
            $img2 = L121c_checkRemoteFile($P_V_CICOURLOFF);


            if($img1 == false || $img2 == false) {

                print "invalid image";
                return false;
            } else {
                print "valid image";
            }
        }

		if(!$P_V_CID){
			$response = generate_response("error", $ERR_MSG_NOTREG);
		}elseif(empty($P_V_ICON) && $P_V_CICON == 0){
			$response = generate_response("error", $ERR_MSG_NOICON);
		}elseif($P_V_CICON == 1 && (empty($P_V_CICOURLON) || empty($P_V_CICOURLOFF))){
			$response = generate_response("error", $ERR_MSG_NOCICON);
		}else{


			if(empty($P_V_ICON) && (!empty($P_V_CICOURLON) || !empty($P_V_CICOURLOFF))){

					$IMG1 = @getimagesize($P_V_CICOURLON);
				$IMG2 = @getimagesize($P_V_CICOURLOFF);

				if(!$IMG1 || !$IMG2){
					$response = generate_response("error", $ERR_MSG_IMGNF);
				}elseif($IMG1[0] > 200 || $IMG1[1] > 100 || $IMG2[0] > 200 || $IMG2[1] > 100){
						$response = generate_response("error", $ERR_MSG_IMGSIZE);
				}else{

				    print "custom here...";


					$BTYPE = 'CUSTOM';
					//$CHECK_ID = $wpdb->get_row("SELECT * FROM " . $wpdb->prefix . "widgetoptions WHERE wid_accountid = " . $getName[0]->id);
					
					if(count($CHECK_ID) > 0){
						$update_data = array(
							'ci_imgpathon'	=> $P_V_CICOURLON,
							'ci_imgpathoff'	=> $P_V_CICOURLOFF,
							'ci_buttontype'	=> $BTYPE
						);
						$update_format = array(
							'%s',
							'%s',
							'%s'
						);
						
						$query_update = $wpdb->update($wpdb->prefix . "chat_icons", $update_data, array('ci_accountid' => $P_V_CID), $update_format, array('%s'));
						if($query_update){
							$response = "success";
						}else{
                            print " err1";

                            $response = generate_response("error", $ERR_MSG_FAIL);
						}

					}else{
						$insert_data = array(
							'ci_accountid'	=> $P_V_CID,
							'ci_imgpathon'	=> $P_V_CICOURLON,
							'ci_imgpathoff'	=> $P_V_CICOURLOFF,
							'ci_buttontype'	=> $BTYPE
						);
						$insert_format = array(
							'%s',
							'%s',
							'%s',
							'%s'
						);

						$insert_datao = array(
							'co_accountid'	=> $P_V_CID,
							'co_chattype'	=> 1,
							'co_proactive'	=> 1,
							'co_exitpop'	=> 1,
							'co_chatformat'	=> 1
						);
						$insert_formato = array(
							'%s',
							'%d',
							'%d',
							'%d',
							'%d'
						);
						$query_insert 	= $wpdb->insert($wpdb->prefix . 'chat_icons', $insert_data, $insert_format);
						$query_inserto 	= $wpdb->insert($wpdb->prefix . 'chat_options', $insert_datao, $insert_formato);

						if($query_insert && $query_inserto){
							$response = "success";
						}else{
                            print " err2";

                            $response = generate_response("error", $ERR_MSG_FAIL);
						}
					}

				}
			}else{

			    print "default here...";

				$BTYPE 		= 'DEFAULT';

				if($P_V_CICON == 1) {
                    $P_V_ICON = $P_V_CICOURLOFF;
                }

				$IMG_ON 	= str_replace("CHATICON-OFF", "CHATICON-ON", $P_V_ICON);
				$IMG_OFF	= $P_V_ICON;


				if(count($CHECK_ID) > 0){

					$update_data = array(
						'ci_imgpathon'	=> $IMG_ON,
						'ci_imgpathoff'	=> $IMG_OFF,
						'ci_buttontype'	=> $BTYPE
					);
					$update_format = array(
						'%s',
						'%s',
						'%s'
					);

					$query_update = $wpdb->update($wpdb->prefix . "chat_icons", $update_data, array('ci_accountid' => $P_V_CID), $update_format, array('%s'));


					print " 
                        icon online = $P_V_CICOURLON <br>
                        icon off = $P_V_CICOURLOFF";
                    // add here to icons library by jesus
                    $check = $wpdb->get_row("SELECT * FROM " . $wpdb->prefix . "chat_icons_library WHERE online_icon='$P_V_CICOURLON' and offline_icon='$P_V_CICOURLOFF'");

                    //                    print "SELECT * FROM " . $wpdb->prefix . "chat_icons_library WHERE online_icon='$P_V_CICOURLON' and offline_icon='$P_V_CICOURLOFF'";
                    //                    print "check now";
                    //                    print_r($check);

                    $add_icon_library = false;
                    if(count($check) < 1) {

                        print "start adding to library ";
                        $add_icon_library = process_icon_add($P_V_CID, $P_V_CICOURLON, $P_V_CICOURLOFF, $BTYPE);
                    }

                    if($query_update and $add_icon_library){
						$response = "success";
					}else{
                        print " err3";
                        $response = generate_response("error", $ERR_MSG_FAIL);
					}

				}else{

					$insert_data = array(
						'ci_accountid'		=> $P_V_CID,
						'ci_imgpathon'		=> $IMG_ON,
						'ci_imgpathoff'	=> $IMG_OFF,
						'ci_buttontype'	=> $BTYPE
					);

					$insert_format = array(
						'%s',
						'%s',
						'%s',
						'%s'
					);

					$insert_datao = array(
						'co_accountid'	=> $P_V_CID,
						'co_chattype'	=> 1,
						'co_proactive'	=> 1,
						'co_exitpop'	=> 1,
						'co_chatformat'	=> 1
					);

					$insert_formato = array(
						'%s',
						'%d',
						'%d',
						'%d',
						'%d'
					);

					$query_insert 	= $wpdb->insert($wpdb->prefix . 'chat_icons', $insert_data, $insert_format);
					$query_inserto 	= $wpdb->insert($wpdb->prefix . 'chat_options', $insert_datao, $insert_formato);

					// add here to icons library by jesus
                    $add_icon_library = process_icon_add($P_V_CID, $P_V_CICOURLON, $P_V_CICOURLOFF, $BTYPE);

                    if($query_insert && $query_inserto && $add_icon_library){
						$response = "success";
					}else{
                        print " err";

                        $response = generate_response("error", $ERR_MSG_FAIL);
					}
				}
			}
		}

		echo $response;
		die();
	}
	add_action('wp_ajax_process_icons', 'process_icons');



	function processscript(){
		//$domainn	= $_POST[ 'domainn' ];
		//$scripturl	= $_POST[ 'scripturl' ];
		//$proactives	= $_POST[ 'proactive' ];
		$color_bg = $_POST['color_bg'];
		$color_text = $_POST['color_text'];
		$clientid	= getCurrentLogggedInAccountId();
		$department_id = $_POST['department_id'];
		$url = getenv('live121support_url');
		$exitpops	= $_POST[ 'exitpopup' ];
		$scriptz	= '
				function load_chat( ){
					var chatURL, po, s;
					var acct = '.$clientid.';
					
					var LHCChatOptions = {};
					LHCChatOptions.opt = {widget_height:340,widget_width:300,popup_height:520,popup_width:500};
								
					var referrer = (document.referrer) ? encodeURIComponent(document.referrer.substr(document.referrer.indexOf(\'://\')+1)) : \'\';
					var location  = (document.location) ? encodeURIComponent(window.location.href.substring(window.location.protocol.length)) : \'\';
					
					po = document.createElement(\'script\');
					po.type = \'text/javascript\'; 
					po.async = true;
					s = document.getElementsByTagName(\'script\')[0];
					
					chatURL = \'//'.$url.'/index.php/chat/getstatus/(click)/internal/(position)/bottom_right/(ma)/br/(top)/350/(units)/pixels/(leaveamessage)/true/(department)/'.$department_id.'?r=\'+referrer+\'&l=\'+location+\'&partner_id=\'+acct;

					po.src = chatURL;					 
					s.parentNode.insertBefore(po, s);
					
					'.($exitpops == 1 ? 'bind_exitpop( );' : '').'}

				function trigger_click( ){
					lh_inst.lh_openchatWindow( );
					setTimeout(function( ) {
						$(\'#lhc_remote_window\').trigger(\'click\');
					}, 1000);}

				function bind_exitpop( ){
					$(window).bind(\'beforeunload\', function( ){
						return \'Please dont go!\';});}';
		$iframesrc 	= htmlentities('<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
			<script src="'.str_replace("includes/","",plugin_dir_url(__FILE__)).'assets/cdn/js/live-121.js"> </script>
			<script type="text/javascript">
				$(function() {
					load_chat();
					$("#live-121-chat-icon img").css("cursor", "pointer");
					$("#lhc_status_container #offline-icon").css("background-color","'.$color_bg.'");
					$("#lhc_status_container #online-icon").css("background-color","'.$color_bg.'");
					$("#lhc_status_container").each(function () {
					    this.style.setProperty( "background-color", "'.$color_bg.'", "important" );
					});
					$("#lhc_status_container h1 span").each(function () {
					    this.style.setProperty( "color", "'.$color_text.'", "important" );
					});
				});
				'.$scriptz.'
			</script>');
		file_put_contents($tfilesrc, $iframesrc, LOCK_EX);
		$contents = file_get_contents($tfilesrc);
		echo $iframesrc;
		die( );
	}
	add_action('wp_ajax_processscript', 'processscript');

	function generate_response($type, $message){
		if($type == "success") $response = "<div class='success'>".$message."</div>";
		else $response = "<div class='error'>".$message."</div>";

		return $response;
	}

	function get_site_client(){
		global $wpdb;
		$PARTNERID	= $_POST['pid'];
		$QUEGETSITE = "SELECT * FROM " . $wpdb->prefix . "clientsites WHERE s_accountid='" . $PARTNERID . "'";
		$RESULTGETS = $wpdb->get_results($QUEGETSITE);

		return $RESULTGETS;
	}

	add_action('wp_ajax_process_domain', 'process_domain');
	function process_domain()
	{
		// Initialized db class
		$chat_queries = new Chat_Queries('wp_clientsites');

		// Delete all the domain by partner id
		$deleted = $chat_queries->wpdb_delete(['s_accountid'=>getCurrentLogggedInAccountId()]);

		if($deleted) {
		    print "successfully deleted old domains";
        }

		// Get serialized post request
		$request = $_REQUEST;


		print_r($request);

		// For each request
		if(!empty($request['pnw_domain_values'])) {
			foreach($request['pnw_domain_values'] as $pnw_domain_value) {
				$chat_queries->wpdb_insert(
					[
						's_accountid'=>getCurrentLogggedInAccountId(),
						's_website' => $pnw_domain_value,
						's_accountidhash' => base64_encode(getCurrentLogggedInAccountId()),
						's_status' => 1
					]
				);
			}

			print "added new domain!";
		}

		return 'success';

	}

	function getCurrentLogggedInAccountId()
	{
		if(getenv('environment') == 'local') {
			return 77333;
		} else {
			$current_user = wp_get_current_user();

			$API_URL	= 'http://api.ontraport.com/1/objects?';

			$API_DATA	= array(
				'objectID'		=> 0,
				'performAll'	=> 'true',
				'sortDir'		=> 'asc',
				'condition'		=> "email='".$current_user->user_email."'",
				'searchNotes'	=> 'true'
			);

			$API_KEY 					  	 = get_field('custom_api_key','option');
			$API_ID						     = get_field('custom_api_id','option');
			$chat_settings_page_title 	     = get_field('chat_settings_page_title','option');
			$chat_settings_title_description = get_field('chat_settings_title_description','option');

			$API_RESULT = op_query($API_URL, 'GET', $API_DATA, $API_ID, $API_KEY);

			$getName = json_decode($API_RESULT);

			return $getName->data[0]->id;
		}
	}

 	add_action('wp_ajax_save_chat_settings', 'save_chat_settings');
 	/**
 	* Chat settings save or update
 	*/
	function save_chat_settings()
	{
		$chat_queries   = new Chat_Queries('wp_chat_options');
		$chat_company_queries = new Chat_Queries('wp_chat_companies');

		$partner_id = getCurrentLogggedInAccountId();

		$request = $_REQUEST;

		$department_id = 1;

		//print "Information<br>";
		//print_r($request);

		$chatSettings = [
		    'color_picker_bg'  => ($request['color_picker_bg'] != null) ? $request['color_picker_bg'] : '#000000',
		    'color_picker_txt' => ($request['color_picker_txt'] != null) ? $request['color_picker_txt'] : '#FFFFFF',
			'co_chatformat'    => ($request['LC_OPT'] != null) ? $request['LC_OPT'] : 1,
			'co_chattype'      => $request['p_cwchat'],
			'co_proactive'     => $request['p_cpactive'],
			'co_exitpop'       => $request['p_cexitpop']
		];

		$results = $chat_queries->wpdb_get_result("select * from wp_chat_options where co_accountid = " . $partner_id);

		if(!empty($results)) {
			$status = $chat_queries->wpdb_update(
				$chatSettings,
				[
					'co_accountid'=> $partner_id
				]
			);

		} else {

            $chatSettings['co_accountid'] = $partner_id;

            //print_r($chatSettings);

            $status = $chat_queries->wpdb_insert($chatSettings);

            if ($status) {
               // print "insert success";
            } else {
               // print "insert failed";
            }
        }

        ////////////////////////////////////////////////////////
        //////////////////////////LIVE 121//////////////////////
        ////////////////////////////////////////////////////////
        $companyInfo = getCompanyNameAndPackageLevel();


		if( connect_live_chat() ) {
		   //print " connected. ";
        } else {
		    //print " not connected. ";
        }

        // Connect live 121 helper
        $live121_queries = new Chat_Queries('lh_companies', connect_live_chat());

		// check if exist already in lh_compaines via partner id
        $isExist = $live121_queries->wpdb_get_result("SELECT * FROM lh_companies where partner_id = " . $partner_id);

        // get current chat icons
        $icons = $chat_queries->wpdb_get_result("select * from wp_chat_icons where ci_accountid = " . $partner_id);

        // get current chat options
        $settings = $chat_queries->wpdb_get_result("select * from wp_chat_options where co_accountid = " . $partner_id);

        // get current sites in testing
        $sites = $chat_queries->wpdb_get_result("select * from wp_clientsites where s_accountid = " . $partner_id);

        // Check if exist or not
        // If not exist then do insert
        // else do update
        if(empty($isExist)) {
           // print " insert ";

            $live121_department = new Chat_Queries('lh_departament', connect_live_chat());

            $incharge_dept = 1; //Admin's department

            $sql = array(
            	'name' => $companyInfo['company'],
            	'email' => $companyInfo['email'],
            	'priority' => 0,
            	'sort_priority' => 0,
            	'department_transfer_id' => $incharge_dept, 
            	'transfer_timeout' => 30,
            	'nc_cb_execute' => 1, 
            	'na_cb_execute' => 1, 
            	'inform_options' => 'a:0:{}',
            	'product_configuration' => '{\"products_enabled\":0,\"products_required\":0}'
            );

            $department_id = $live121_department->wpdb_insert( $sql );

//            print " department id $department_id";

            $data = [
                'domain' => serialize($sites),
                'icons' => serialize($icons),
                'settings' => serialize($settings),
                'name' => $companyInfo['company'],
                'package' => $companyInfo['packageLevel'],
                'partner_id' => $partner_id,
                'department_id' => $department_id
            ];

            $status = $live121_queries->wpdb_insert( $data );

            if($department_id > 0) {
                // insert worpdress lh_companies
                $chat_company_queries->wpdb_insert(['partner_id' => $partner_id, 'department_id' => $department_id]);
            }

        } else {
            //print " update";
            $data =  [
                'domain' => serialize($sites),
                'icons' => serialize($icons),
                'settings' => serialize($settings),
                'name' => $companyInfo['company'],
                'package' => $companyInfo['packageLevel'],
            ];

            $status = $live121_queries->wpdb_update(
                $data
                ,
                [
                    'partner_id' => $partner_id
                ]
            );

//            print " partner id " . $partner_id;
//            global $wpdb;
//            $department = $wpdb->get_results("select * from wp_chat_companies where partner_id = " . $partner_id, ARRAY_A);
//            print_r($department);
//EXIT;
            // get department id in wordpress
             $department = $chat_company_queries->wpdb_get_result("select * from wp_chat_companies where partner_id = " . $partner_id);
             $department_id = (!empty($department[0]['department_id']) ? $department[0]['department_id'] : $department_id);
//            print_r($department);
//            EXIT;
//            print " dp $department_id";
//            exit;
            //            print $department_id ;
        }
        /*
        if($status) {
            print "<br> success";
        } else {
            print "<br> failed";
        }

         print " unserialized";
         print_r(unserialize($isExist[0]['settings']));


        print_r($data);*/
        echo  '&lt;div id="live-121-chat-icon" partner-id="' . getCurrentLogggedInAccountId() . '" department-id="' . $department_id . '" >&lt;/div>';
        die();
	}

	function getCompanyNameAndPackageLevel()
    {
        if (getenv('environment') == 'local') {
            $response['company'] = 'This is company name';
            $response['packageLevel'] = 'This is package level';
            $response['email'] = 'test@email.com';

            return $response;
        } else {
            // print "user is logged in ";

            $current_user = wp_get_current_user();
            //$customAPIKEY  = get_field('custom_api_key','option');// name of the admin
            //$customAPIID  = get_field('custom_api_id','option');// Email Title for the admin
            //echo "Email Address: " . $current_user->user_email;
            //$postargs = "http://api.ontraport.com/1/objects?objectID=0&performAll=true&sortDir=asc&condition=email%3D'testing@umbrellasupport.co.uk'&searchNotes=true";
            $postargs = "http://api.ontraport.com/1/objects?objectID=0&performAll=true&sortDir=asc&condition=email%3D'" . $current_user->user_email . "'&searchNotes=true";

            $session = curl_init();
            curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($session, CURLOPT_URL, $postargs);
            //curl_setopt ($session, CURLOPT_HEADER, true);
            curl_setopt($session, CURLOPT_HTTPHEADER, array(
                'Api-Appid:2_7818_AFzuWztKz',
                'Api-Key:fY4Zva90HP8XFx3'
            ));

            $response = curl_exec($session);
            curl_close($session);

            //header("Content-Type: text");
            //echo "CODE: " . $response;

            $getName = json_decode($response);

            $response['company'] = $getName->data[0]->company;
            $response['packageLevel'] = $getName->data[0]->f1548;

            return $response;
        }
    }

	function connect_live_chat()
    {
        if (getenv('environment') == 'local') {
            // print "local";
            return new wpdb(getenv('local_db_user'), getenv('local_db_pw'), getenv('local_db_name'), getenv('local_db_host'));
        } else {
            // print "online";
            return new wpdb(getenv('live_db_user'), getenv('live_db_pw'), getenv('live_db_name'), getenv('live_db_host'));
        }
    }
?>