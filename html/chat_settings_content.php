<div class="chat-settings-container">
         		
				<form action="#" method="post" id="pnw_chat_settings" >

				<input type="hidden" value="save_chat_settings" name="action" />

					<table cellpadding="10" cellspacing="">
						<tr>
							<td>
								<b>
									Chat Color
								</b>
							</td>
							<td>
							
							    <input id="cp7" type="text" class="form-control" value="pink" data-toggle="tooltip" data-placement="top" title="you can type color names" />
							
							
								<br />
							</td>
						</tr>
						<tr>
							<td>
								<b>
									Window Chat Type
								</b>
							</td>
							<td>
								<div class="switch-field">
									<input type="radio" id="S_TWC_L1" name="p_cwchat" value="1" <?php if (isset($getRowsCO)) { echo ($getRowsCO->co_chattype == 1 ? "checked" : "");} ?> required/>
									<label for="S_TWC_L1">Pop-up</label>&nbsp;&nbsp;
									<input type="radio" id="S_TWC_R1" name="p_cwchat" value="0" <?php if (isset($getRowsCO)) { echo ($getRowsCO->co_chattype == 0 ? "checked" : "");} ?>/>
									<label for="S_TWC_R1">Window</label>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<b>
									Enable Pro-Active Popup Invitation
								</b>
							</td>
							<td>
								<div class="switch-field">
									<input type="radio" id="S_TWC_L2" name="p_cpactive" value="1" <?php if (isset($getRowsCO)) { echo ($getRowsCO->co_proactive == 1 ? "checked" : "");} ?> required/>
									<label for="S_TWC_L2">Yes</label>&nbsp;&nbsp;
									<input type="radio" id="S_TWC_R2" name="p_cpactive" value="0" <?php if (isset($getRowsCO)) { echo ($getRowsCO->co_proactive == 0 ? "checked" : "");} ?> />
									<label for="S_TWC_R2">No</label>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<b>
									Enable Pop-up on Page Close
								</b>
							</td>
							<td> 
								<div class="switch-field">
									<input type="radio" id="S_TWC_L3" name="p_cexitpop" value="1" <?php if (isset($getRowsCO)) { echo ($getRowsCO->co_exitpop == 1 ? "checked" : "");} ?> required/>
									<label for="S_TWC_L3">Yes</label>&nbsp;&nbsp;
									<input type="radio" id="S_TWC_R3" name="p_cexitpop" value="0" <?php if (isset($getRowsCO)) { echo ($getRowsCO->co_exitpop == 0 ? "checked" : "");} ?> />
									<label for="S_TWC_R3">No</label>
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="2">

							
								<div id="pnw-chat-settings-loader" class="preloader" >  
									<!-- <img src="http://testing.umbrellasupport.co.uk/wp-content/uploads/2016/07/preload.gif" />  -->
								</div>
								<div class="save-btn-wrapper">
									<input type="button" id="bigbutton_settings"  onclick="proceeChatSettings()" value="Save and Continue"> 
								</div>
							</td>
						</tr>
					</table>
				</form> 
         	</div>