<form id="manageicons" type="post" action="">

          	<div class="icon-container">
          		<div class="row">
          			<div class="col-md-6">
          				<div class="icon-wrapper">
          					<div class="icon-box">
          						<div class="md-radio">
								    <input id="1" type="radio" name="g">
								    <label for="1">
								    	<img class=" icon-img" src="<?php echo plugins_url( 'images/chat-icons/1-CHATICON-ON.gif', __FILE__ )?>" alt="">
									<img class=" icon-img" src="<?php echo plugins_url( 'images/chat-icons/1-CHATICON-OFF.gif', __FILE__ )?>" alt="">
								    </label>
								  </div>
          					</div>

          				</div>
          				<div class="icon-wrapper">
          					<div class="icon-box">
          						 <div class="icon-box">
          						<div class="md-radio">
								    <input id="2" type="radio" name="g">
								    <label for="2">
								    	<img class=" icon-img" src="<?php echo plugins_url( 'images/chat-icons/2-CHATICON-ON.gif', __FILE__ )?>" alt="">
									<img class=" icon-img" src="<?php echo plugins_url( 'images/chat-icons/2-CHATICON-OFF.gif', __FILE__ )?>" alt="">
								    </label>
								  </div>
          					</div>
          					</div>
          				</div>
          				<div class="icon-wrapper">
          					<div class="icon-box">
          						 <div class="md-radio">
								    <input id="3" type="radio" name="g">
								    <label for="3">
								    	<img class=" icon-img" src="<?php echo plugins_url( 'images/chat-icons/3-CHATICON-ON.gif', __FILE__ )?>" alt="">
									<img class=" icon-img" src="<?php echo plugins_url( 'images/chat-icons/3-CHATICON-OFF.gif', __FILE__ )?>" alt="">
								    </label>
								  </div>
          					</div>
          				</div>
          				<div class="icon-wrapper">
          					<div class="icon-box">
          						 <div class="md-radio">
								    <input id="4" type="radio" name="g">
								    <label for="4">
								    	<img class=" icon-img" src="<?php echo plugins_url( 'images/chat-icons/4-CHATICON-ON.gif', __FILE__ )?>" alt="">
									<img class=" icon-img" src="<?php echo plugins_url( 'images/chat-icons/4-CHATICON-OFF.gif', __FILE__ )?>" alt="">
								    </label>
								  </div>
          					</div>
          				</div>
          				<div class="icon-wrapper">
          					<div class="icon-box">
          						 <div class="md-radio">
								    <input id="5" type="radio" name="g">
								    <label for="5">
								    	<img class=" icon-img" src="<?php echo plugins_url( 'images/chat-icons/5-CHATICON-ON.gif', __FILE__ )?>" alt="">
									<img class=" icon-img" src="<?php echo plugins_url( 'images/chat-icons/5-CHATICON-OFF.gif', __FILE__ )?>" alt="">
								    </label>
								  </div>
          					</div>
          				</div>	
          			</div>
          			<div class="col-md-6">
          				<div class="icon-wrapper">
          					<div class="icon-box">
          						 <div class="md-radio">
								    <input id="6" type="radio" name="g">
								    <label for="6">
								    	<img class=" icon-img" src="<?php echo plugins_url( 'images/chat-icons/6-CHATICON-ON.gif', __FILE__ )?>" alt="">
									<img class=" icon-img" src="<?php echo plugins_url( 'images/chat-icons/6-CHATICON-OFF.gif', __FILE__ )?>" alt="">
								    </label>
								  </div>
          					</div>
          				</div>
          				<div class="icon-wrapper">
	          				<div class="icon-box">
	          						 <div class="md-radio">
								    <input id="7" type="radio" name="g">
								    <label for="7">
								    	<img class=" icon-img" src="<?php echo plugins_url( 'images/chat-icons/7-CHATICON-ON.jpg', __FILE__ )?>" alt="">
									<img class=" icon-img" src="<?php echo plugins_url( 'images/chat-icons/7-CHATICON-OFF.jpg', __FILE__ )?>" alt="">
								    </label>
								  </div>
	          					</div>
          					</div>
          				<div class="icon-wrapper">
          					<div class="icon-box">
          						 <div class="md-radio">
								    <input id="8" type="radio" name="g">
								    <label for="8">
								    	<img class=" icon-img" src="<?php echo plugins_url( 'images/chat-icons/8-CHATICON-ON.gif', __FILE__ )?>" alt="">
									<img class=" icon-img" src="<?php echo plugins_url( 'images/chat-icons/8-CHATICON-OFF.gif', __FILE__ )?>" alt="">
								    </label>
								  </div>
          					</div>
          				</div>
          				<div class="icon-wrapper">
          					<div class="icon-box">
          						 <div class="md-radio">
								    <input id="9" type="radio" name="g">
								    <label for="9">
								    	<img class=" icon-img" src="<?php echo plugins_url( 'images/chat-icons/9-CHATICON-ON.gif', __FILE__ )?>" alt="">
									<img class=" icon-img" src="<?php echo plugins_url( 'images/chat-icons/9-CHATICON-OFF.gif', __FILE__ )?>" alt="">
								    </label>
								  </div>
          					</div>
          				</div>
          				<div class="icon-wrapper">
	          				<div class="icon-box">
	          						<div class="md-radio">
								    <input id="10" type="radio" name="g">
								    <label for="10">
								    	<img class=" icon-img" src="<?php echo plugins_url( 'images/chat-icons/10-CHATICON-ON.gif', __FILE__ )?>" alt="">
									<img class=" icon-img" src="<?php echo plugins_url( 'images/chat-icons/10-CHATICON-OFF.gif', __FILE__ )?>" alt="">
								    </label>
								  </div>
	          				</div>
          				</div>
          				</div>
          			<div class="col-md-12">
          				<div class="icon-wrapper">
	          				<div class="icon-box">
	          						 <div class="md-radio">
								    <input id="11" type="radio" name="g">
								    <label for="11">
								    	<img class=" icon-img" src="<?php echo plugins_url( 'images/chat-icons/11-CHATICON-ON.gif', __FILE__ )?>" alt="">
									<img class=" icon-img" src="<?php echo plugins_url( 'images/chat-icons/11-CHATICON-OFF.gif', __FILE__ )?>" alt="">
								    </label>
								  </div>
	          				</div>
          				</div>
          			</div>

				<div class="col-md-12 own-btn-container">
					<div class="col-md-6">	
						<div class="select-btn-wrapper">
							<p>Have your own button?</p>
							<div class="switch-field" style="padding: 10px 15px 50px 10px;"> 
								<input type="radio" id="S_OB_L1" name="p_obut" value="0" checked>
								<label id="Lbl_no" for="S_OB_L1">No</label>&nbsp;&nbsp; 
								<input type="radio" id="S_OB_R1" name="p_obut" value="1"> 
								<label id="Lbl_yes" for="S_OB_R1">Yes</label>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="input-container">
							<p>
                        <span style="color: #008000;"><strong>Online Image:</strong></span>
                        <input type="text" class="urltext" name="p_cctxt1" id="p_ctxt1" value="<?php echo ($getUserCI->ci_buttontype == "CUSTOM" && $getRowsW->wid_imgpathon ? $getRowsW->wid_imgpathon : ""); ?>">
                        <!-- <span style="font-size:11px">Example: <i>http://domain.com/images/online.gif</i></span>-->
                    </p>
                    <p>
                        <span style="color: #ff0000;"><strong>Offline Image:</strong></span>
                        <input type="text" class="urltext" name="p_cctxt2" id="p_ctxt2" value="<?php echo ($getRowsCI->ci_buttontype == "CUSTOM" && $getRowsCI->ci_imgpathoff ? $getRowsCI->ci_imgpathoff : ""); ?>">
                        <!-- <span style="font-size:11px">Example: <i>http://domain.com/images/offline.gif</i></span>-->
                    </p> 
						</div>
					</div>
				</div>

				<div class="col-md-12">
					<div class="save-btn-wrapper-chat-icon">
						<input type="button" id="bigbutton_chat" value="Save and Continue"> 
					</div>
				</div>

          		</div>
          	</div>


	</form>