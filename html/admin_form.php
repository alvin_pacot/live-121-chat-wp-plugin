<div class="wrap">
	<h1>Live 121 admin settings</h1>
	<form method="post" action="options.php">
		<?php 
			settings_fields( 'live_121_settings' );
			do_settings_sections( 'live_121_settings' );
		?>
		<table class="form-table">
			<br>
			This is the configuration of live 121 web chat settings.
			<br>
			<tr valign="top">
		        <th scope="row">Username</th>
		        <td><input class="regular-text" type="text" id="lh_username" name="lh_username" value="<?php echo esc_attr( get_option('lh_username') ); ?>"/></td>
	        </tr>
			<tr valign="top">
		        <th scope="row">Host Url</th>
		        <td><input class="regular-text" type="text" id="lh_host_url" name="lh_host_url" value="<?php echo esc_attr( get_option('lh_host_url') ); ?>"/>
		        <small>Note: Always include the "/" on the end.</small></td>
	        </tr>
			<tr valign="top">
		        <th scope="row">API Key</th>
		        <td><input class="regular-text" type="text" id="lh_api_key" name="lh_api_key" value="<?php echo esc_attr( get_option('lh_api_key') ); ?>"/></td>
	        </tr>
	        <br>
				Database
			<br>
	        <tr valign="top">
		        <th scope="row"></th>
		        <td><input class="regular-text" type="text" id="lh_db_name" name="lh_db_name" value="<?php echo esc_attr( get_option('lh_username') ); ?>"/></td>
	        </tr>
			<tr valign="top">
		        <th scope="row">Host Url</th>
		        <td><input class="regular-text" type="text" id="lh_host_url" name="lh_host_url" value="<?php echo esc_attr( get_option('lh_host_url') ); ?>"/>
		        <small>Note: Always include the "/" on the end.</small></td>
	        </tr>
			<tr valign="top">
		        <th scope="row">API Key</th>
		        <td><input class="regular-text" type="text" id="lh_api_key" name="lh_api_key" value="<?php echo esc_attr( get_option('lh_api_key') ); ?>"/></td>
	        </tr>
		</table>
		<?php submit_button(); ?>
	</form>
	<form method="post" action="options.php">
		<?php 
			settings_fields( 'ontraport_settings' );
			do_settings_sections( 'ontraport_settings' );
		?>
		<table class="form-table">
			<br>
			This is the configuration of live 121 web chat settings.
			<br>
			<tr valign="top">
		        <th scope="row">API Key</th>
		        <td><input class="regular-text" type="text" id="ontra_api_key" name="ontra_api_key" value="<?php echo esc_attr( get_option('ontra_api_key') ); ?>"/></td>
	        </tr>
	        <tr valign="top">
		        <th scope="row">API ID</th>
		        <td><input class="regular-text" type="text" id="ontra_api_id" name="ontra_api_id" value="<?php echo esc_attr( get_option('ontra_api_id') ); ?>"/></td>
	        </tr>
		</table>
		<?php submit_button(); ?>
	</form>
	<h1>Add image icons</h1>
	<form>
		<table class="form-table">
			<br>
			Upload icons using wp media upload and add the icon urls here.
			<br>
			<tr valign="top">
		        <th scope="row">Icon URL (Online):</th>
		        <td><input class="regular-text" type="text" id="online-icon-url"/></td>
	        </tr>
	        <tr valign="top">
		        <th scope="row">Icon URL (Offline):</th>
		        <td><input class="regular-text" type="text" id="offline-icon-url"/></td>
	        </tr>
		</table>
		<p class="submit">
       		<button type="button" class="button button-primary" id="saveIconsAjax">Add Icon</button>
       	</p>
	</form>
</div>

<!-- For testing purposes only -->
<!--a id="callAjax" class="button button-primary" href="#">Call</a-->