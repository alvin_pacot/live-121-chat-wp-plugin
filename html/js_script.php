<script type="text/javascript">
jQuery(document).ready(function($) {
	/**
	 *
	 * Saving Icons to database
	 *
	 */
	 $('#saveIconsAjax').click(function () {
	 	if($('#online-icon-url').val() == '' || $('#offline-icon-url').val() == ''){
	 		swal('Invalid url!', "Please, fill both url's", "error");
	 	} else {
	 		var online_url = $('#online-icon-url').val();
	 		var offline_url = $('#offline-icon-url').val();

	 		var data = {
				'action': 'save_icons',
				'online_url': online_url,
				'offline_url': offline_url
			};

			$.ajax({
				type: "POST",
				url: ajaxurl, 
				data: data, 
				success: function (data) {
					console.log(data);
				},
				error: function (data) {
					console.log(data)
				}
			});
	 	}
	 });
});
</script>