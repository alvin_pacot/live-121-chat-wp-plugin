-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 01, 2017 at 10:33 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `richard_wordpress_debug`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_chat_icons_library`
--

CREATE TABLE `wp_chat_icons_library` (
  `id` int(11) NOT NULL,
  `partner_id` varchar(25) NOT NULL,
  `online_icon` text NOT NULL,
  `offline_icon` text NOT NULL,
  `byte` varchar(20) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wp_chat_icons_library`
--

INSERT INTO `wp_chat_icons_library` (`id`, `partner_id`, `online_icon`, `offline_icon`, `byte`, `date`) VALUES
(3, '77333', 'http://testing.umbrellasupport.co.uk/wp-content/uploads/2016/07/9-CHATICON-ON.gif', 'http://testing.umbrellasupport.co.uk/wp-content/uploads/2016/07/9-CHATICON-OFF.gif', 'DEFAULT', '2017-08-01 08:17:54'),
(4, '77333', 'http://testing.umbrellasupport.co.uk/wp-content/uploads/2016/07/7-CHATICON-ON.jpg', 'http://testing.umbrellasupport.co.uk/wp-content/uploads/2016/07/7-CHATICON-OFF.jpg', 'DEFAULT', '2017-08-01 08:18:12'),
(5, '77333', 'http://testing.umbrellasupport.co.uk/wp-content/uploads/2016/07/3-CHATICON-ON.gif', 'http://testing.umbrellasupport.co.uk/wp-content/uploads/2016/07/3-CHATICON-OFF.gif', 'DEFAULT', '2017-08-01 08:18:32'),
(6, '77333', 'http://testing.umbrellasupport.co.uk/wp-content/uploads/2016/07/11-CHATICON-ON.gif', 'http://testing.umbrellasupport.co.uk/wp-content/uploads/2016/07/11-CHATICON-OFF.gif', 'DEFAULT', '2017-08-01 08:18:41'),
(7, '77333', 'http://testing.umbrellasupport.co.uk/wp-content/uploads/2016/07/1-CHATICON-ON.gif', 'http://testing.umbrellasupport.co.uk/wp-content/uploads/2016/07/1-CHATICON-OFF.gif', 'DEFAULT', '2017-08-01 08:19:11'),
(8, '77333', 'http://testing.umbrellasupport.co.uk/wp-content/uploads/2016/07/8-CHATICON-ON.gif', 'http://testing.umbrellasupport.co.uk/wp-content/uploads/2016/07/8-CHATICON-OFF.gif', 'DEFAULT', '2017-08-01 08:19:31'),
(9, '77333', 'http://testing.umbrellasupport.co.uk/wp-content/uploads/2016/07/4-CHATICON-ON.gif', 'http://testing.umbrellasupport.co.uk/wp-content/uploads/2016/07/4-CHATICON-OFF.gif', 'DEFAULT', '2017-08-01 08:19:54'),
(10, '77333', 'http://testing.umbrellasupport.co.uk/wp-content/uploads/2016/07/2-CHATICON-ON.gif', 'http://testing.umbrellasupport.co.uk/wp-content/uploads/2016/07/2-CHATICON-OFF.gif', 'DEFAULT', '2017-08-01 08:20:10'),
(16, '77333', 'http://testing.umbrellasupport.co.uk/wp-content/uploads/2016/07/10-CHATICON-ON.gif', 'http://testing.umbrellasupport.co.uk/wp-content/uploads/2016/07/10-CHATICON-OFF.gif', 'DEFAULT', '2017-08-01 08:21:17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_chat_icons_library`
--
ALTER TABLE `wp_chat_icons_library`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_chat_icons_library`
--
ALTER TABLE `wp_chat_icons_library`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
