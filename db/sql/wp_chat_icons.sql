-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 01, 2017 at 10:31 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `richard_wordpress_debug`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_chat_icons`
--

CREATE TABLE `wp_chat_icons` (
  `ci_ID` bigint(20) NOT NULL,
  `ci_accountid` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `ci_imgpathon` text COLLATE latin1_general_ci NOT NULL,
  `ci_imgpathoff` text COLLATE latin1_general_ci NOT NULL,
  `ci_buttontype` varchar(20) COLLATE latin1_general_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `wp_chat_icons`
--

INSERT INTO `wp_chat_icons` (`ci_ID`, `ci_accountid`, `ci_imgpathon`, `ci_imgpathoff`, `ci_buttontype`) VALUES
(18, '77333', 'http://testing.umbrellasupport.co.uk/wp-content/uploads/2016/07/10-CHATICON-ON.gif', 'http://testing.umbrellasupport.co.uk/wp-content/uploads/2016/07/10-CHATICON-OFF.gif', 'DEFAULT');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_chat_icons`
--
ALTER TABLE `wp_chat_icons`
  ADD PRIMARY KEY (`ci_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_chat_icons`
--
ALTER TABLE `wp_chat_icons`
  MODIFY `ci_ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
