-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 04, 2017 at 01:32 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `richard_wordpress_debug`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_chat_options`
--

CREATE TABLE `wp_chat_options` (
  `co_ID` bigint(20) NOT NULL,
  `co_accountid` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `co_chattype` varchar(150) COLLATE latin1_general_ci NOT NULL,
  `co_proactive` smallint(6) NOT NULL,
  `co_exitpop` smallint(6) NOT NULL,
  `co_chatformat` smallint(6) NOT NULL,
  `color_picker_bg` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `color_picker_txt` varchar(50) COLLATE latin1_general_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_chat_options`
--
ALTER TABLE `wp_chat_options`
  ADD PRIMARY KEY (`co_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_chat_options`
--
ALTER TABLE `wp_chat_options`
  MODIFY `co_ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=134;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
