-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 01, 2017 at 10:31 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `richard_wordpress_debug`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_clientsites`
--

CREATE TABLE `wp_clientsites` (
  `s_ID` bigint(20) NOT NULL,
  `s_accountid` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `s_accountidhash` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `s_website` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `s_status` smallint(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_clientsites`
--

INSERT INTO `wp_clientsites` (`s_ID`, `s_accountid`, `s_accountidhash`, `s_website`, `s_status`) VALUES
(4, '77332', '7840431e46f0ebd7b683f39c36d78436', 'umbrellasupport.co.uk', 1),
(5, '77332', '7840431e46f0ebd7b683f39c36d78436', 'trusted-tradesmen.org.uk', 1),
(44, '77514', 'Nzc1MTQ=', 'http://www.google.com', 1),
(45, '77514', 'Nzc1MTQ=', 'http://www.nicetest.com', 1),
(146, '77333', 'NzczMzM=', 'http://localhost/erwin/richard/debug/live121support.com', 1),
(145, '77333', 'NzczMzM=', 'http://amazing-grace.com', 1),
(144, '77333', 'NzczMzM=', 'http://www.google.com', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_clientsites`
--
ALTER TABLE `wp_clientsites`
  ADD PRIMARY KEY (`s_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_clientsites`
--
ALTER TABLE `wp_clientsites`
  MODIFY `s_ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=147;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
