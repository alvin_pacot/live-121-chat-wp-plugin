<?php 
/*
 * Template Name: Live 121 test
 * Description: A Page Template for Live Chat Settings
 */

get_header(); 

require 'vendor/autoload.php';

$factory = new \Database\Connectors\ConnectionFactory();

$connection = $factory->make(array(
    'driver'    => 'mysql', 
    'database' 	=> 'live121supportdb', //'db655765888'
    'host'      => 'localhost', //'db655765888.db.1and1.com'
    'username'  => 'root', //'dbo655765888'
    'password'  => '', //'ZwokV*#%8STsIx/1'
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
));

$result = $connection->fetchAll("SELECT * FROM lh_companies");

if($result['partner_id'] == )
print_r($result);
?>

<div>
	<button type="button" id="step3" class="btn btn-primary">Step 3 -> Step 4</button>
</div>

<script type="text/javascript">
	$(function () {
		$('#step3').click(function () {
			$.ajax({
				url: "<?php echo admin_url('admin-ajax.php'); ?>",
				method: "POST",
				data: {
					'action': 'htmlcode'
				}
			}).done(function (data) {
				console.log(data)
			});
		});
	});
	
</script>

<?php get_footer(); ?>