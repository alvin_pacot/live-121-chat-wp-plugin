$(function () {

	// Init Bootstrap color picker
	$('#cp7').colorpicker({
		 customClass: 'colorpicker-2x',
            sliders: {
                saturation: {
                    maxLeft: 200,
                    maxTop: 200
                },
                hue: {
                    maxTop: 200
                },
                alpha: {
                    maxTop: 200
                }
            }
	});

	// To get the value of the picked color
	// $("#cp7").val();

	// Tooltip for color picker
	 $('[data-toggle="tooltip"]').tooltip();

	// console.log("ready!");
	$("#bigbutton_chat").on("click", function (event) {
		var inputVal1 = $("#p_ctxt1").val();
		var inputVal2 = $("#p_ctxt2").val();
		var own_button = $("input[name='p_obut']").val();
		//console.log(own_button);
		if (own_button == 1) {
			//console.log(inputVal1);
			if (inputVal1 == "" && inputVal2 == "") {
				swal('Invalid input!', 'Please check fill in all required fields!', 'error');
			} else {
				//ajax here where we save the inputs to the database
				// var next = $('#myTab li.active').next();
				// next.length ? next.find('a').click() : $('#myTab li a')[0].click();
			}
		} else {}
	});

	$(document).on('show.bs.tab', '.nav-tabs-responsive [data-toggle="tab"]', function (e) {
		var $target = $(e.target);
		var $tabs = $target.closest('.nav-tabs-responsive');
		var $current = $target.closest('li');
		var $parent = $current.closest('li.dropdown');
		$current = $parent.length > 0 ? $parent : $current;
		var $next = $current.next();
		var $prev = $current.prev();
		var updateDropdownMenu = function updateDropdownMenu($el, position) {
			$el.find('.dropdown-menu').removeClass('pull-xs-left pull-xs-center pull-xs-right').addClass('pull-xs-' + position);
		};

		$tabs.find('>li').removeClass('next prev');
		$prev.addClass('prev');
		$next.addClass('next');

		updateDropdownMenu($prev, 'left');
		updateDropdownMenu($current, 'center');
		updateDropdownMenu($next, 'right');
	});

	jQuery("#S_OB_L1").on("click", function (event) {
		$("#p_ctxt1").prop('disabled', true);
		$("#p_ctxt1").val(null);
		$("#p_ctxt2").prop('disabled', true);
		$("#p_ctxt2").val(null);
		$("#S_OB_L1").prop('checked', true);
		$("#S_OB_R1").prop('checked', false);
		$('.urltext').css('background-color', '#e4e4e4');
		$("#Lbl_yes").css('background-color', '#e4e4e4');
		$("#Lbl_yes").css('color', 'black');
		$("#Lbl_no").css('color', 'white');
		$("#Lbl_no").css('background-color', '#be0000');
	});

	jQuery("#S_OB_R1").on("click", function (event) {
		$("#p_ctxt1").prop('disabled', false);
		$("#p_ctxt1").val(null);
		$("#p_ctxt2").prop('disabled', false);
		$("#p_ctxt2").val(null);
		$("#S_OB_L1").prop('checked', false);
		$("#S_OB_R1").prop('checked', true);
		$('.urltext').css('background-color', '#ffffff');
		$("#Lbl_no").css('background-color', '#e4e4e4');
		$("#Lbl_yes").css('color', 'white');
		$("#Lbl_no").css('color', 'black');
		$("#Lbl_yes").css('background-color', '#be0000');
	});

	var proceed_status = false;


	$("#bigbutton_chat").on("click", function (event) {

		var inputVal1 = $("#p_ctxt1").val();
		var inputVal2 = $("#p_ctxt2").val();

		if (inputVal1 && inputVal2) {
			// var next = $('#myTab li.active').next();
			// next.length ? next.find('a').click() : $('#myTab li a')[0].click();
		} else if (inputVal1 && inputVal2 == "" || inputVal1 == "" && inputVal2) {
			// alert("please fill up the details");
		} else {
			$("input:radio").each(function () {
				var name = jQuery(this).attr("name");
				var chat_radio_name = "g";

				if (name === "g") {
					if ($("input:radio[name=" + chat_radio_name + "]").is(":checked")) {


						// alert when a radion button is checked
						//alert(name);
						// proceed_status = true;
						// $("#auth-tab").removeClass("disabledTab");
						//
						// //  $('#myTab a:last').tab('show'); will open the last tab
						// var next = $('#myTab li.active').next();
						// next.length ? next.find('a').click() : $('#myTab li a')[0].click();

						return false;

						// open next tab
					}
					// alert when no icon is checked
					// alert("Please choose an icon");
					// return false;
				}
			});
		}
	});

	$("#bigbutton_website").on("click", function (e) {

	});

	$("#bigbutton_settings").on("click", function (e) {
		e.preventDefault();

		$("#script-tab").removeClass("disabledTab");
		// show next tab
		$("#myTab a:last").tab("show");
	});

	// When click Yes or No for "Have your own buttons?", it should remove the selected icon and restore to normal ui
	// $("#S_OB_R1, #S_OB_L1").click(function(){

	// 	// Set not clicked select in step 1 to visible and can be clickable again
	// 	$("#pnw-select-div label").each(function( index  ){
	// 		$(this).css('z-index','2');
	// 	});

	// })

	// Click the select button in step 1
	$("#pnw-select-div #pnw-select").click(function () {

		// Set not clicked select in step 1 to visible and can be clickable again
		$("#pnw-select-div label").each(function (index) {
			$(this).css('z-index', '2');
		});

		// Set clicked selected as in backward
		$(this).css('z-index', '0');
	});

	$('ul.ctabs li').click(function () {

		// check tab with
		if ($(this).attr('data-visited') == 'active') {

			var tab_id = $(this).attr('data-tab');
			$('ul.ctabs li').removeClass('current');
			$('.ctab-content').removeClass('current');
			$(this).addClass('current');
			$("#" + tab_id).addClass('current');
		}
	});

	disablefields();
});

/*
var LHCChatOptions = {};
LHCChatOptions.opt = {widget_height:340,widget_width:300,popup_height:520,popup_width:500};
(function() {
	var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
	var referrer = (document.referrer) ? encodeURIComponent(document.referrer.substr(document.referrer.indexOf('://')+1)) : '';
	var location  = (document.location) ? encodeURIComponent(window.location.href.substring(window.location.protocol.length)) : '';
	po.src = '//live121support.com/index.php/chat/getstatus/(click)/internal/(position)/bottom_right/(ma)/br/(top)/350/(units)/pixels/(leaveamessage)/true/(department)/3?r='+referrer+'&l='+location;
	var s = document.getElementsByTagName('script')[0]; s.parent Node.insertBefore(po, s);
})();*/

function disablefields() {
	$("#p_ctxt1").prop('disabled', true);
	$("#p_ctxt1").val(null);
	$("#p_ctxt2").prop('disabled', true);
	$("#p_ctxt2").val(null);
	$("#S_OB_L1").prop('checked', true);
	$("#S_OB_R1").prop('checked', false);
	$('.urltext').css('background-color', '#e4e4e4');
}

/**
 * Used in step 1, when selected the checkbox it should automatically assign the selected image source to a hidden field id chat-image-selected
 * @param onlineImgSrc
 * @param offlineImgSrc
 */
function selectCheckBoxIcon(onlineImgSrc, offlineImgSrc) {
	document.getElementById('chat-image-selected').value  = onlineImgSrc;
}


function processicons() {



	// console.log("ds");

    var managechat = $('#manageicons').serialize();

  	 //console.log(managechat);

	// $("input[name='g']")

	//console.log( $("#p_ctxt1").val()  + $("#p_ctxt2").val());

	//console.log(" add new icon disabled " + $('#p_ctxt1').attr('disabled'));
	// if field is disabled then dont check if selected icon or not



	if($('#p_ctxt1').attr('disabled') == "disabled") {

        //Check if icon selected or not

        // console.log(" checked " + $('input[name=g]:checked', '#manageicons').val());

        // $("#lcs-icons").each(function (i) {
        // console.log(" i " + i);
        // var name = jQuery(this).attr("name");
        // console.log(name)
        //
        // console.log(" name = " + name);
        // if (name === "g") {
        //     alert("Please choose an icon 1");
        //     return false;
        // }

        // });
        var checkBox = $('input[name=g]:checked', '#manageicons').val();
		//console.log(checkBox);

        if(checkBox !== "g1") {
            alert("Please choose an icon");
            return false;
		}

    } else if($("#p_ctxt1").val() == '' || $("#p_ctxt2").val()  == '') {
        // If adding new icon found empty then this alert should show and stop from debug
        alert("Please provide a valid icon url");
        return false;
    }




    // show pre loader
	$('#preloader').show();

	jQuery.ajax({

		type: "POST", // HTTP method POST or GET

		url: $("#lcs_admin_ajax").val(), //Where to make Ajax calls
		//dataType:"text", // Data type, HTML, json etc.

		data: managechat,

		success: function success(response) {

			//console.log(" response = " + response);

			if(response.indexOf('invalid image') > -1) {

				alert("Image url is not found, please check it again.");

            } else {

				//console.log("success");

                proceed_status = true;
                $("#auth-tab").removeClass("disabledTab");

                //console.log(" add new icon "  + $('#p_ctxt1').attr('disabled'));

                // if($('#p_ctxt1').attr('disabled') == "disabled") {
                    if ($("input:radio[name=g]").is(":checked")) {
                        //  $('#myTab a:last').tab('show'); will open the last tab
                        var next = $('#myTab li.active').next();
                        next.length ? next.find('a').click() : $('#myTab li a')[0].click();
                    // }
                }

				// changeTab('open tab 2');

				// Set current tab visited and can be visited by time to time
				$("#menu-tab-2").attr('data-visited', 'active');

				// add data-visited attribute to active in step 1

			}

			// else {
            //
			// 	console.log("failed");
			// 	$('#error_container').html(response);
			// }

			//get_script();
		},

		error: function error(xhr, ajaxOptions, thrownError) {

			alert("Error: " + thrownError);
		},

		complete: function complete() {

			$('#preloader').hide();
		}

	});

	return false;
}

/**
 *  Step 2 field validation
 */
function processWebsite(action, id) {

	switch (action) {

		case 'Add Domain':

			var domain = document.getElementById('pdw-domain-validation').value;
			var total = document.getElementById('pnw-total-site');
			var total_counter = document.getElementById('pnw-total-site-counter');

			// Hide empty text when no domain found and added new domain
			$(".dataTables_empty").css('display', 'none');

			if (isValidURL(domain)) {

				if (total_counter.value < 5) {

					// Increment total sites
					total.value = parseInt(total.value) + 1;

					// Increment total sites counter
					total_counter.value = parseInt(total_counter.value) + 1;

					// Append new html
					$("#web_list tbody").append("<tr id='pnw-domain-container-" + total.value + "'><td>" + total.value + "</td><td><span id='pnw-domain-text-" + total.value + "' >" + domain + "</span><input type='hidden' value='" + domain + "' name='pnw_domain_values[]' id='pnw-domain-value-" + total.value + "' /></td><td>Active</td><td><button type='button'  onclick='processWebsite(\"Edit Domain\", " + total.value + ")' >Edit</button></td><td><button type='button' onclick='processWebsite(\"Delete Domain\", " + total.value + ")'>Delete</button></td></tr>");

					// Clean the domain field
					$('#pdw-domain-validation').val('');
				} else {

					alert(total_counter.value + " domain allowed for now.");

					// display message if exceed 5 domain
					// $("#pnw-adding-domain-message").html("<div class='alert alert-danger'><b>5 domain allowed for now.</b></div>");
				}

				//console.log("Domain is valid and now try to insert to database");
			} else {
				alert("Please provide valid domain.");
			}

			break;

		case 'Edit Domain':

			//console.log(" id " + id + " Domain Value " + $("#pnw-domain-value-" + id).val());

			// Get domain from display text and Add domain in the field to update
			$('#pdw-domain-validation').val($("#pnw-domain-value-" + id).val());

			// Assign id recent edited for update hit button
			$edit_id = id;

			// show update button and hide add button
			$("#pnw-domain-add-button").css('display', 'none');
			$("#pnw-domain-update-button").css('display', 'block');

			break;

		case 'Update Domain':

			// Update domain display text
			$("#pnw-domain-text-" + $edit_id).html($('#pdw-domain-validation').val());

			// Update domain input hidden text
			$("#pnw-domain-value-" + $edit_id).val($('#pdw-domain-validation').val());

			// Clean the domain field
			$('#pdw-domain-validation').val('');

			// Show add button and hide update button
			$("#pnw-domain-add-button").css('display', 'block');
			$("#pnw-domain-update-button").css('display', 'none');

			break;

		case 'Delete Domain':

			if (confirm('Are you sure you want to delete this domain?')) {

				$("#pnw-domain-container-" + id).html("");

				var total_counter = document.getElementById('pnw-total-site-counter');

				total_counter.value = parseInt(total_counter.value) - 1;
			}

			break;

		case 'Save And Continue':

			// Get all the domain in serialized format
			var data = $("#pnw_form").serialize();



			//console.log(" data " + data);
			// Show loader
			$("#pnw-save-domain-loader").css("display", "block");

			// Send all the domain via REST API_KEYI in serialized format
			jQuery.ajax({

				type: "POST", // HTTP method POST or GET

				url: $("#lcs_admin_ajax").val(), // Where to make Ajax calls

				data: data,

				success: function success(response) {


                    //console.log("open new tab");

                    $("#settings-tab").removeClass("disabledTab");
                    var next = $('#myTab li.active').next();
                    next.length ? next.find('a').click() : $('#myTab li a')[0].click();


					//console.log("response " + response);

					if (response == 'success' || response == 0) {
						// Alert response
						//console.log(response);

						// Set current tab visited and can be visited by time to time
						$("#menu-tab-3").attr("data-visited", 'active');

						// Proceed to tab 4
						// changeTab('open tab 3');


					}

					// Hide loader
					$("#pnw-save-domain-loader").css("display", "none");
				},

				error: function error(xhr, ajaxOptions, thrownError) {

					alert("Error: " + thrownError);
					$("#pnw-save-domain-loader").css("display", "none");
				},

				complete: function complete() {

					$("#pnw-save-domain-loader").css("display", "none");
				}

			});

			break;

		default:

			// console.log("Default");
			// console.log("Domain start validation..");
			// var domain = document.getElementById('pdw-domain-validation').value;
			// if (isValidURL(domain)) {

			// 	console.log("valid domain");
			// 	changeTab('open tab 3');
			// } else {

			// 	console.log("not valid domain");
			// }
			break;
	}
}

/**
 *  Step 3 hit save then should validate some fields
 */
function proceeChatSettings() {

	var datas = {
        'exitpopup': 1,
        'department_id': 1,
        'action': 'processscript',
        'color_bg': $("#color_bg").val(),
        'color_text': $("#color_text").val(),
    };

    jQuery.ajax({
        url: $("#lcs_admin_ajax").val(),
        data: datas,
        method: "POST",
    }).done(function (msg) {
        $('#foo').append(msg);
    });

	//console.log("proceess chat settings");
	// Set current tab visited and can be visited by time to time
	//$("#menu-tab-4").attr("data-visited", "active");

	//changeTab('open tab 4');


	// Get all the domain in serialized format
	var data = $("#pnw_chat_settings").serialize();

	//console.log(data);

	// Show loader
	$("#pnw-chat-settings-loader").css("display", "block");

	// Send all the domain via REST API_KEYI in serialized format
	jQuery.ajax({

		type: "POST", // HTTP method POST or GET

		url: $("#lcs_admin_ajax").val(), // Where to make Ajax calls

		data: data,

		success: function success(response) {
			console.log(response);
			$('#bar').append(response);

			if (response == 'success' || response == 0) {
				// Set current tab visited and can be visited by time to time
				$("#menu-tab-3").attr("data-visited", 'active');
				$("#menu-tab-4").attr("data-visited", 'active');

				changeTab('open tab 4');
			}

			// Hide loader
			$("#pnw-chat-settings-loader").css("display", "none");
		},

		error: function error(xhr, ajaxOptions, thrownError) {

			alert("Error: " + thrownError);
			$("#pnw-chat-settings-loader").css("display", "none");
		},

		complete: function complete() {

			$("#pnw-chat-settings-loader").css("display", "none");
		}

	});
}

/**
 *  Helper Javascript
 */
function isValidURL(str) {

	var regex = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;

	if (!regex.test(str)) {

		//console.log("Please enter valid URL.");
		return false;
	} else {

		return true;
	}
}

function changeTab(tab) {

	//console.log(tab);

	/** Set all tabs as not selected */
	$('ul.ctabs li').removeClass('current');

	/** Set all tab content as not selected */
	$('.ctab-content').removeClass('current');

	if (tab == 'open tab 2') {

		/** Set second tab as selected  */
		$('#menu-tab-2').addClass('current');
		$("#tab-2").addClass('current');
	} else if (tab == 'open tab 3') {

		/** Set third tab as selected  */
		$('#menu-tab-3').addClass('current');
		$("#tab-3").addClass('current');
	} else if (tab == 'open tab 4') {

		/** Set fourth tab as selected  */
		$('#menu-tab-4').addClass('current');
		$("#tab-4").addClass('current');
	}
}