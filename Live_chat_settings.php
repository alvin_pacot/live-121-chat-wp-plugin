<?php

/*
Plugin Name: Live 121 Web Chat
Description: Plugin for the Live Chat Settings
Author: Umbrella Support Web Development Team
Version: 1.0
*/
	require 'vendor/autoload.php';

	use Dotenv\Dotenv;

	$dotenv = new Dotenv(__DIR__);
	$dotenv->load();

	
	require('Livechatsettings_pagetemplater.php');
    require('includes/live_chat_settings_helper.php');
    require_once("db/wpdb_chat_queries.class.php");
    require_once("db/LiveChatSettings.php");
    require('includes/functions_chat.php');


/*
	Create Admin Menu
*/
	function LiveChatSettings_AdminMenu() {
		$adminMenu = add_menu_page("Live Chat Settings Plugin","Live Chat", "manage_options","LiveChatSetting_Options","LiveChatSettings_AdminPageView","dashicons-admin-site");
	}

	add_action("admin_menu", "LiveChatSettings_AdminMenu");
	add_action( 'admin_init', 'register_my_setting' );
/**
 *
 * Register settings API
 *
 */
	function register_my_setting() {
		register_setting( 'live_121_settings', 'lh_api_key');
		register_setting( 'live_121_settings', 'lh_host_url');
		register_setting( 'live_121_settings', 'lh_username');
		register_setting( 'live_121_settings', 'lh_db_name');
		register_setting( 'live_121_settings', 'lh_db_username');
		register_setting( 'live_121_settings', 'lh_db_password');
		register_setting( 'live_121_settings', 'lh_db_host');

		register_setting( 'ontraport_settings', 'ontra_api_key');
		register_setting( 'ontraport_settings', 'ontra_api_id');
	}
/*
	Create Live Chat Settings page 
*/
	function LiveChatSettings_AdminPageView() {
		include('Livechatsettings_adminpage.php');
	}

/*
	Register stylesheets
*/
	function theme_styles() {
		wp_enqueue_style( 'tab-pane-version','https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css' );
		wp_enqueue_style( "font-awesome",'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
		wp_enqueue_style( "sweetalert",'https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.6/sweetalert2.css');
		wp_enqueue_style( 'bootstrap-colorpicker', plugin_dir_url(__FILE__) . 'node_modules/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css');
		wp_enqueue_style( "app", plugin_dir_url(__FILE__) . 'dist/app.css');

	}

	add_action( 'wp_enqueue_scripts', 'theme_styles');

	function theme_js() {

		global $wp_scripts;
		wp_enqueue_script('jquery.2','http://code.jquery.com/jquery-2.0.3.min.js');
		wp_enqueue_script( "clipboard",'https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.7.1/clipboard.min.js');
		wp_enqueue_script( "bootstrap",'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js');
		wp_enqueue_script( "sweetalert_js",'https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.6/sweetalert2.js');
		wp_enqueue_script( 'bootstrap-colorpicker-js', plugin_dir_url(__FILE__) .'node_modules/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js');
		wp_enqueue_script( "tab-pane", plugin_dir_url(__FILE__) . 'src/app.js',array('jquery'));

	}

	add_action( 'wp_enqueue_scripts', 'theme_js');

/**
 *
 * Register css and js on admin
 *
 */

	function load_custom_wp_admin_style() {
		wp_enqueue_style( "sweetalert",'https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.6/sweetalert2.css');
        wp_enqueue_script( "sweetalert_js",'https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.6/sweetalert2.js');
	}
	add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );


/**
 *
 * Plugin activation
 *
 */

	function myplugin_activate() {
		global $wpdb;

		$charset_collate = $wpdb->get_charset_collate();

		$chat_options = $wpdb->prefix . "chat_options";

		$chat_options_sql = "CREATE TABLE $chat_options (
		  `co_ID` bigint(20) NOT NULL AUTO_INCREMENT,
		  `co_accountid` varchar(10) COLLATE latin1_general_ci NOT NULL,
		  `co_chattype` varchar(150) COLLATE latin1_general_ci NOT NULL,
		  `co_proactive` smallint(6) NOT NULL,
		  `co_exitpop` smallint(6) NOT NULL,
		  `co_chatformat` smallint(6) NOT NULL,
		  PRIMARY KEY  (co_ID)
		) $charset_collate;";

		maybe_create_tables($chat_options, $chat_options_sql);

		$clientsites = $wpdb->prefix . "clientsites";

		$clientsites_sql = "CREATE TABLE $clientsites (
		  `s_ID` bigint(20) NOT NULL AUTO_INCREMENT,
		  `s_accountid` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
		  `s_accountidhash` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
		  `s_website` text COLLATE utf8mb4_unicode_ci NOT NULL,
		  `s_status` smallint(6) NOT NULL,
		  PRIMARY KEY  (s_ID)
		) $charset_collate;";

	    maybe_create_tables($clientsites, $clientsites_sql);

	    $chaticons = $wpdb->prefix . "chat_icons";

		$chaticons_sql = "CREATE TABLE $chaticons (
		  `ci_ID` bigint(20) NOT NULL AUTO_INCREMENT,
		  `ci_accountid` varchar(10) COLLATE latin1_general_ci NOT NULL,
		  `ci_imgpathon` text COLLATE latin1_general_ci NOT NULL,
		  `ci_imgpathoff` text COLLATE latin1_general_ci NOT NULL,
		  `ci_buttontype` varchar(20) COLLATE latin1_general_ci NOT NULL,
		  PRIMARY KEY  (ci_ID)
		) $charset_collate;";
	    	
	   	maybe_create_tables($chaticons, $chaticons_sql);
	}
	register_activation_hook( __FILE__, 'myplugin_activate' );

/**
 *
 * Check table if exist
 *
 */

	function maybe_create_tables($table_name, $create_ddl) {
    	global $wpdb;
	 
	    $query = $wpdb->prepare( "SHOW TABLES LIKE %s", $wpdb->esc_like( $table_name ) );
	 
	    if ( $wpdb->get_var( $query ) == $table_name ) {
	        return true;
	    }
	 
	    // Didn't find it try to create it..
	    $wpdb->query($create_ddl);
	 
	    // We cannot directly tell that whether this succeeded!
	    if ( $wpdb->get_var( $query ) == $table_name ) {
	        return true;
	    }
	    return false;	
	}


/**
 *
 * Ajax testing
 *
 */
	add_action( 'admin_footer', 'my_action_javascript' ); // Write our JS below here

	function my_action_javascript() { 
		include('html/js_script.php');
	}

/**
 *
 * Ajax save Action
 *
 */
	add_action( 'wp_ajax_save_icons', 'save_icons' );

	function save_icons() {
		global $wpdb;
		$online_url = $_POST['online_url'];
		$offline_url = $_POST['offline_url'];
		$insert = $wpdb->insert(
			$wpdb->prefix.'chat_icons_library', 
			array(
				'online_url' => $online_url, 
				'offline_url' => $offline_url
				)
			);

		if($insert){
			echo 'true';
		} else {
			if($wpdb->last_error !== '')
			    echo $wpdb->last_error;
			else
				echo 'no error';
		}
		exit();
	}

/**
 *
 * WP remote get
 *
 */	
	add_action('wp_ajax_htmlcode', 'htmlcode');

    function htmlcode() {

		/**$connection = $factory->make(array(
		    'driver'    => 'mysql',
		    'database' 	=> 'db655765888',
		    'host'      => 'db655765888.db.1and1.com',
		    'username'  => 'dbo655765888',
		    'password'  => 'ZwokV*#%8STsIx/1',
		    'charset'   => 'utf8',
		    'collation' => 'utf8_unicode_ci',
		));*/

		
		die( );
	}
	
?>