<?php

function L121c_checkRemoteFile($url)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    // don't download content
    curl_setopt($ch, CURLOPT_NOBODY, 1);
    curl_setopt($ch, CURLOPT_FAILONERROR, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    if(curl_exec($ch)!==FALSE)
    {
        return true;
    }
    else
    {
        return false;
    }
}

if(L121c_checkRemoteFile('http://testing.umbrellasupport.co.uk/wp-content/uploads/2016/07/10-CHATICON-ON.gif')){

    print "This is an image";

} else {

    print "This is not image";
}

