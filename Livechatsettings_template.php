<?php

/*
 * Template Name: Live Chat Settings Template
 * Description: A Page Template for Live Chat Settings
 */
get_header();

//require_once ('src/live_chat_settings_ajax.php');
//    require_once ('includes/live_chat_settings_ajax.php');
//    require_once ('includes/live_chat_settings_helper.php');

if(!is_user_logged_in()) {
//
    echo '<center><h1>Please login to view this page</h1></center>';
//
} else {

    $current_user = wp_get_current_user();

    $API_URL	= 'http://api.ontraport.com/1/objects?';

    $API_DATA	= array(
        'objectID'		=> 0,
        'performAll'	=> 'true',
        'sortDir'		=> 'asc',
        'condition'		=> "email='".$current_user->user_email."'",
        'searchNotes'	=> 'true'
    );

    if(pnw_is_local() == true) {

        /**
         * Ge gamit ni sa ubos ug sa akong local, so kong mawala ni mag error akoa nga side
         */

        $API_KEY 						=  'Kiok5B2tzM00Oqf';
        $API_ID							 = '2_7818_ubHppKG8C';
        $chat_settings_page_title   	 = 'Live Chat Settings Title';
        $chat_settings_title_description = 'Live Chat Settings Desc';
        $getName->data[0]->id            = 77333;

    } else {

        /**
         * Alvins code for ontraport query,
         * E dire lang pag code sa imo pre or e butang siya ug lain nga condition na sa imo lang kai ma apiktohan akoang side na coding
         * kanang naa sa babaw gi gamit na sa akong local
         * so kailangan sa nimo humanun tanan ni nga side ayha nato e implement para goods ang flow sa dev
         */
        $current_user = wp_get_current_user();
        //dd($current_user);

    }




    $QUEGETSITE = "SELECT * FROM " . $wpdb->prefix . "clientsites WHERE s_accountid='" . $getName->data[0]->id . "'";
    $RESULTGETS = $wpdb->get_results($QUEGETSITE);

    $getUserCI	= "SELECT * FROM ".$wpdb->prefix."chat_icons WHERE ci_accountid = '".$getName->data[0]->id."'";
    $getRowsCI	= $wpdb->get_row($getUserCI);

    $getUserCO	= "SELECT * FROM ".$wpdb->prefix."chat_options WHERE co_accountid = '".$getName->data[0]->id."'";
    $getRowsCO	= $wpdb->get_row($getUserCO);

    // pnw_print_r_pre($getRowsCO);

    /**
     * Jesus Functions
     */
    $liveChatSettings	= new LiveChatSettings(new CHAT_QUERIES\Chat_Queries('wp_clientsites'), 'wp_clientsites');
    $domains = $liveChatSettings->getClientSites(getCurrentLogggedInAccountId());
    $domain_total  = count($domains);

    print " <br><br><br><br>total domain " . $domain_total;

    // print " co_chatformat = " . $getRowsCO->co_chatformat;


    /**
     * Get all images from icon libraries
     */
    $query 	= "SELECT * FROM ".$wpdb->prefix."chat_icons_library ORDER BY id DESC";
    $images = $wpdb->get_results($query);


    /**
     *
     */
    //    print "<pre>";
    //    print_r($images);
    //    print "</pre>";

    //    'p_clientid=77333
    //    &p_cctxt1=http%3A%2F%2Flocalhost%2Ferwin%2Frichard%2Fdebug%2Fwordpress-debug%2Fwp-content%2Fplugins%2Flive-121-chat-wp-plugin%2Fimages%2Fchat-icons%2F1-CHATICON-ON.gif
    //    &p_cctxt2=http%3A%2F%2Flocalhost%2Ferwin%2Frichard%2Fdebug%2Fwordpress-debug%2Fwp-content%2Fplugins%2Flive-121-chat-wp-plugin%2Fimages%2Fchat-icons%2F1-CHATICON-OFF.gif
    //    &p_obut=1
    //    &action=process_icons'

    //    'p_clientid=77333
    //    &action=process_icons
    //    &p_cicon=http%3A%2F%2Flocalhost%2Ferwin%2Frichard%2Fdebug%2Fwordpress-debug%2Fwp-content%2Fplugins%2Flive-121-chat-wp-plugin%2Fimages%2Fchat-icons%2F1-CHATICON-ON.gif
    //    &p_obut=1
    //    &p_cctxt1=http%3A%2F%2Flocalhost%2Ferwin%2Frichard%2Fdebug%2Fwordpress-debug%2Fwp-content%2Fplugins%2Flive-121-chat-wp-plugin%2Fimages%2Fchat-icons%2F1-CHATICON-ON.gif
    //    &p_cctxt2=http%3A%2F%2Flocalhost%2Ferwin%2Frichard%2Fdebug%2Fwordpress-debug%2Fwp-content%2Fplugins%2Flive-121-chat-wp-plugin%2Fimages%2Fchat-icons%2F1-CHATICON-OFF.gif'

?>
    <div class="wrapper">


        <div id="hidden-info">
            <input type="hidden" value="<?php echo admin_url('admin-ajax.php'); ?>" id="lcs_admin_ajax" />

        </div>


        <div class="container" id="live_chat_container">

            <div id="narrow-browser-alert" class="alert alert-info text-center">
                <strong class="test">LIVE WEB CHAT SETTINGS</strong> </div>

            <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
                <ul id="myTab" class="nav nav-tabs nav-tabs-responsive" role="tablist">

                    <li role="presentation"  class="active" id="chat-tab"    >
                        <a href="#chat-icons" role="tab" id="chat-icons-tab" data-toggle="tab"  aria-controls="chat-icons">
                            <i class="fa fa-comments-o" aria-hidden="true"></i>
                            <span class="text">CHAT ICONS</span>
                            <span class="step-text">(Step 1)</span>
                        </a>
                    </li>

                    <li role="presentation" id="auth-tab"   class="next disabledTab">
                        <a href="#websites" role="tab"  data-visited="" data-toggle="tab" aria-controls="websites">
                            <i class="fa fa-globe" aria-hidden="true"></i>
                            <span class="text">AUTHORIZED DOMAIN NAME</span>
                            <span class="step-text">(Step 2)</span>
                        </a>
                    </li>

                    <li role="presentation" id="settings-tab"  class="next disabledTab">
                        <a href="#chat-settings" role="tab" id="chat-settings-tab" data-visited="" data-toggle="tab" aria-controls="chat-settings">
                            <i class="fa fa-cog" aria-hidden="true"></i>
                            <span class="text">CHAT SETTINGS</span>
                            <span class="step-text">(Step 3)</span>
                        </a>
                    </li>

                    <li role="presentation" id="script-tab" class="next disabledTab">
                        <a href="#generated-script" role="tab" id="generated-script-tab" data-visited="" data-toggle="tab" aria-controls="generated-script">
                            <i class="fa fa-sticky-note-o" aria-hidden="true"></i>
                            <span class="text">GENERATED SCRIPT</span>
                            <span class="step-text">(Step 4)</span>
                        </a>
                    </li>

                </ul>
                <div id="myTabContent" class="tab-content">

                    <div role="tabpanel" class="tab-1 tab-pane fade  in active" id="chat-icons" aria-labelledby="chat-icons-tab">

                        <form id="manageicons" type="post" action="">


                            <input type="hidden" id="p_clientid" name="p_clientid" value="<?php echo $getName->data[0]->id; ?>" />
                            <input type="hidden" name="action" value="process_icons" />





                            <div class="icon-container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="hidden" name="p_cicon" value="<?php print $getRowsCI->ci_imgpathon; ?>" id="chat-image-selected" />

                                        <div class="row">
                                            <?php
                                            foreach($images as $index => $img) {

                                                $chatIconOnline  = $img->online_icon;
                                                $chatIconOffline  = $img->offline_icon;

                                                $checked = '';

                                                // If found
                                                if($getRowsCI->ci_imgpathon == $img->online_icon) {
                                                    $checked = 'checked';
                                                }

                                                ?>

                                                <!-- Edit this html to loook 2 column -->

                                                <div class="col-md-6">
                                                    <div class="icon-wrapper">
                                                        <div class="icon-box">
                                                            <div class="md-radio">
                                                                <input  <?php print $checked; ?>  id="<?php print $index; ?>" type="radio" name="g" value="g1" onclick="selectCheckBoxIcon('<?php print $chatIconOffline; ?>', '<?php print $chatIconOnline; ?>')"  />
                                                                <label for="<?php print $index; ?>">
                                                                    <img class=" icon-img" src="<?php echo $chatIconOnline; ?>" alt="" />
                                                                    <img class=" icon-img" src="<?php echo $chatIconOffline; ?>" alt="" />
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>             
                                         </div>
                                    </div>
                                    <div class="col-md-12 own-btn-container">
                                        <div class="col-md-6">
                                            <div class="select-btn-wrapper">

                                                <p>Have your own button?</p>
                                                <div class="switch-field" style="padding: 10px 15px 50px 10px;">

                                                    <input type="radio" id="S_OB_L1" name="p_obut" value="0" checked>
                                                    <label id="Lbl_no" for="S_OB_L1">No</label>&nbsp;&nbsp;

                                                    <input type="radio" id="S_OB_R1" name="p_obut" value="1">
                                                    <label id="Lbl_yes" for="S_OB_R1">Yes</label>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="input-container">
                                                <p>
                                                    <span style="color: #008000;"><strong>Online Image:</strong></span>
                                                    <input type="text" class="urltext" name="p_cctxt1" id="p_ctxt1" value="<?php echo ($getUserCI->ci_buttontype == "CUSTOM" && $getRowsW->wid_imgpathon ? $getRowsW->wid_imgpathon : ""); ?>">
                                                    <!-- <span style="font-size:11px">Example: <i>http://domain.com/images/online.gif</i></span>-->
                                                </p>
                                                <p>
                                                    <span style="color: #ff0000;"><strong>Offline Image:</strong></span>
                                                    <input type="text" class="urltext" name="p_cctxt2" id="p_ctxt2" value="<?php echo ($getRowsCI->ci_buttontype == "CUSTOM" && $getRowsCI->ci_imgpathoff ? $getRowsCI->ci_imgpathoff : ""); ?>">
                                                    <!-- <span style="font-size:11px">Example: <i>http://domain.com/images/offline.gif</i></span>-->
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="save-btn-wrapper-chat-icon">
                                            <!-- <input type="button" id="bigbutton_chat" value="Save and Continue" name="p_submit" onclick="processicons()" >-->
                                            <input type="button"  id="bigbutton_chat"  value="Save and Continue" name="p_submit" onclick="processicons()" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div role="tabpanel" class="tab-2 tab-pane fade" id="websites" aria-labelledby="websites_tab">
                        <div class="container-website">
                            <div  style="padding: 47px;">
                                <table cellpadding="10" cellspacing="">
                                    <tr>
                                        <td>
                                            <b>Input Website Address:</b>
                                            <b>http://</b>
                                            <br>
                                            <br>
                                            <div class="row">

                                                <div class="col-md-6">

                                                    <input id="pdw-domain-validation" type="text" name="" value="" class="search" style="padding: 9px;padding:10px;font-size:14px;  ">

                                                </div>

                                                <div class="col-md-2">
                                                    <button type="button" id="pnw-domain-add-button" class="query-wrapper query-add" style="display: block; ">
                                                        <img src="<?php echo plugins_url('images/add.png',__FILE__ );?>" onclick="processWebsite('Add Domain')" style="height: 23px;">
                                                    </button>

                                                    <button type="button" id="pnw-domain-update-button" class="query-wrapper query-add" style="display: none;">
                                                        <img  src="<?php echo plugins_url('images/edit.png',__FILE__ );?>"onclick="processWebsite('Update Domain')" style="height: 23px;"></button><div id="pnw-adding-domain-message">
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <input type="hidden" id="pnw-total-site" value="<?php print $domain_total; ?>" />
                                <input type="hidden" id="pnw-total-site-counter" value="<?php print $domain_total; ?>" />

                                <form id="pnw_form" action="" method="POST" >

                                    <input type="hidden" value="process_domain" name="action" />

                                    <table id="web_list" class="display table" cellspacing="0">
                                        <thead>
                                        <tr>
                                            <th >#</th>
                                            <th  >Website Address</th>
                                            <th  >Status</th>
                                            <th class="no-sort text-center" colspan="2">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $counter = 0;
                                        foreach($domains as $domain) {
                                            $counter++;
                                            $partner_id = $domain['s_ID'];
                                            $domain     = $domain['s_website'];

                                            echo "<tr id='pnw-domain-container-$partner_id'><td>$counter</td><td><span id='pnw-domain-text-$partner_id' >$domain</span><input type='hidden' value='$domain' name='pnw_domain_values[]' id='pnw-domain-value-$partner_id' /></td><td>Active</td><td><button type='button'  onclick='processWebsite(\"Edit Domain\", $partner_id )' >Edit</button></td><td><button type='button' onclick='processWebsite(\"Delete Domain\", $partner_id )'>Delete</button></td></tr>";

                                        } ?>
                                        </tbody>
                                    </table>
                                </form>
                                <div class="auth-btn-container">
                                    <input style="padding: 15px 55px;" type="button" id="bigbutton_website" onclick="processWebsite('Save And Continue')" value="Save and Continue">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-3 tab-pane fade" id="chat-settings" aria-labelledby="chat-settings-tab">
                        <div class="chat-settings-container">

                            <form action="#" method="post" id="pnw_chat_settings" >

                                <input type="hidden" value="save_chat_settings" name="action" />

                                <table cellpadding="10" cellspacing="">

                                    <tr>
                                        <td>
                                            <b>
                                                Chat Color Background
                                            </b>
                                        </td>
                                        <td>


                                            <div id="cp2" class="input-group colorpicker-component">
                                                <input id="color_bg" type="text" value="<?php echo (!empty($getRowsCO->color_picker_bg) ? $getRowsCO->color_picker_bg : '#000000');  ?>" class="form-control" name="color_picker_bg" />
                                                <span class="input-group-addon"><i></i></span>
                                            </div>
                                            <script>
                                                $(function() {
                                                    $('#cp2').colorpicker();
                                                });
                                            </script>

                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <b>
                                                Chat Color Text
                                            </b>
                                        </td>
                                        <td>


                                            <div id="cp3" class="input-group colorpicker-component">
                                                <input id="color_txt" type="text" value="<?php echo (!empty($getRowsCO->color_picker_txt) ? $getRowsCO->color_picker_txt : '#FFFFFF');  ?>" class="form-control" name="color_picker_txt" />
                                                <span class="input-group-addon"><i></i></span>
                                            </div>
                                            <script>
                                                $(function() {
                                                    $('#cp3').colorpicker();
                                                });
                                            </script>

                                        </td>
                                    </tr>



                                    <tr>
                                        <td>
                                            <b>
                                                Window Chat Type
                                            </b>
                                        </td>
                                        <td>
                                            <div class="switch-field">
                                                <input type="radio" id="S_TWC_L1" name="p_cwchat" value="1" <?php echo ($getRowsCO->co_chattype == 1 ? "checked" : ""); ?> required/>
                                                <label for="S_TWC_L1">Pop-up</label>&nbsp;&nbsp;
                                                <input type="radio" id="S_TWC_R1" name="p_cwchat" value="0" <?php echo ($getRowsCO->co_chattype == 0 ? "checked" : ""); ?>/>
                                                <label for="S_TWC_R1">Window</label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>
                                                Enable Pro-Active Popup Invitation
                                            </b>
                                        </td>
                                        <td>
                                            <div class="switch-field">
                                                <input type="radio" id="S_TWC_L2" name="p_cpactive" value="1" <?php echo ($getRowsCO->co_proactive == 1 ? "checked" : ""); ?> required/>
                                                <label for="S_TWC_L2">Yes</label>&nbsp;&nbsp;
                                                <input type="radio" id="S_TWC_R2" name="p_cpactive" value="0" <?php echo ($getRowsCO->co_proactive == 0 ? "checked" : ""); ?> />
                                                <label for="S_TWC_R2">No</label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>
                                                Enable Pop-up on Page Close
                                            </b>
                                        </td>
                                        <td>
                                            <div class="switch-field">
                                                <input type="radio" id="S_TWC_L3" name="p_cexitpop" value="1" <?php echo ($getRowsCO->co_exitpop == 1 ? "checked" : ""); ?> required/>
                                                <label for="S_TWC_L3">Yes</label>&nbsp;&nbsp;
                                                <input type="radio" id="S_TWC_R3" name="p_cexitpop" value="0" <?php echo ($getRowsCO->co_exitpop == 0 ? "checked" : ""); ?> />
                                                <label for="S_TWC_R3">No</label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">

                                            <div id="pnw-chat-settings-loader" class="preloader" >
                                                <!-- <img src="http://testing.umbrellasupport.co.uk/wp-content/uploads/2016/07/preload.gif" />  -->
                                            </div>
                                            <div class="save-btn-wrapper">
                                                <input type="button" id="bigbutton_settings"  onclick="proceeChatSettings()" value="Save and Continue">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-4 tab-pane fade" id="generated-script" aria-labelledby="generated-script-tab">
                        <div class="script-container">
                            <div class="copy-btn">
                                <a href="#" class="btn" data-clipboard-action="copy" id="btncopy" data-clipboard-target="#foo">
                                   Copy to clipboard
                                </a>
                            </div>
                            <br>
                            <center>
                                <h3>Copy and paste this to your footer.</h3>
                            </center>
                            <pre id="foo" class="script-style">

                			</pre>
                            <br>
                            <center>
                                <h3>Copy and paste this anyware to your page.</h3>
                            </center>
                            <pre id="bar" class="script-style">
                                
                            </pre>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="//code.jquery.com/jquery-1.12.3.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" class="init">
        jQuery.noConflict();
        jQuery('#web_list').DataTable({
            responsive: true,
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": true,
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false,
            } ]
        });

        var clip = new Clipboard('#btncopy');

        clip.on('success', function(e) {
            $('.copied').show();
            $('.copied').fadeOut(1000);
        });

    </script>
    <?php
}
get_footer();